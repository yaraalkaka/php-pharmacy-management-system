--
-- PostgreSQL database dump
--

-- Dumped from database version 13.3
-- Dumped by pg_dump version 13.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: company; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.company (
    cmp_id integer NOT NULL,
    cmp_name character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    contract_date date,
    address character varying(50),
    phone character varying(50),
    valid boolean
);


ALTER TABLE public.company OWNER TO postgres;

--
-- Name: company_cmp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.company_cmp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_cmp_id_seq OWNER TO postgres;

--
-- Name: company_cmp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.company_cmp_id_seq OWNED BY public.company.cmp_id;


--
-- Name: employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.employee (
    emp_id integer NOT NULL,
    emp_name character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    password character varying(50),
    phone character varying(50) NOT NULL,
    address character varying(50),
    title text
);


ALTER TABLE public.employee OWNER TO postgres;

--
-- Name: employee_emp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.employee_emp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.employee_emp_id_seq OWNER TO postgres;

--
-- Name: employee_emp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.employee_emp_id_seq OWNED BY public.employee.emp_id;


--
-- Name: medicine; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medicine (
    drug_name character varying(200) NOT NULL,
    cmp_name character varying(200),
    class character varying(200),
    ingredient character varying(200),
    prod_date date,
    valid boolean,
    drug_id integer NOT NULL,
    quantity integer
);


ALTER TABLE public.medicine OWNER TO postgres;

--
-- Name: medicine_drug_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medicine_drug_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.medicine_drug_id_seq OWNER TO postgres;

--
-- Name: medicine_drug_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.medicine_drug_id_seq OWNED BY public.medicine.drug_id;


--
-- Name: records; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.records (
    drug_id integer,
    op_type character varying(6),
    rec_date date,
    rec_id integer NOT NULL,
    doctor character varying(100),
    pharm character varying(100),
    quantity integer
);


ALTER TABLE public.records OWNER TO postgres;

--
-- Name: records_rec_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.records_rec_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.records_rec_id_seq OWNER TO postgres;

--
-- Name: records_rec_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.records_rec_id_seq OWNED BY public.records.rec_id;


--
-- Name: company cmp_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company ALTER COLUMN cmp_id SET DEFAULT nextval('public.company_cmp_id_seq'::regclass);


--
-- Name: employee emp_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employee ALTER COLUMN emp_id SET DEFAULT nextval('public.employee_emp_id_seq'::regclass);


--
-- Name: medicine drug_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicine ALTER COLUMN drug_id SET DEFAULT nextval('public.medicine_drug_id_seq'::regclass);


--
-- Name: records rec_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records ALTER COLUMN rec_id SET DEFAULT nextval('public.records_rec_id_seq'::regclass);


--
-- Data for Name: company; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.company (cmp_id, cmp_name, email, contract_date, address, phone, valid) FROM stdin;
1	Kmart Corporation	hblacrkborne0@fotki.com	2021-04-12	392 Mandrake Park	(860) 7084801	t
2	EMD Serono, Inc.	ddavidrdi1@de.vu	2021-07-25	8884 Thierer Way	(165) 4291964	t
3	RECORDATI RARE DISEASES, INC.	afrorwen2@bloomberg.com	2021-06-22	54 Ludington Junction	(196) 3088543	t
4	Newton Laboratories, Inc.	rwynrch3@google.de	2020-11-20	9909 Butterfield Trail	(553) 1929452	t
6	REMEDYREPACK INC.	dcreperl5@home.pl	2020-12-22	6 Carpenter Point	(135) 8906255	t
7	Torrent Pharmaceuticals Limited	areern6@apple.com	2021-06-22	757 Warbler Terrace	(315) 3648047	t
8	Natural Creations, Inc.	scaprini7@ifeng.com	2021-07-19	831 American Ash Parkway	(107) 8752725	t
9	Par Pharmaceutical Inc.	jsmithrers8@berkeley.edu	2021-09-13	4 East Crossing	(488) 7050575	t
10	WOCKHARDT LIMITED	amackettricrk9@weebly.com	2021-01-22	690 Hagan Center	(213) 5122804	t
11	West-ward Pharmaceutical Corp	ptarppora@php.net	2021-01-07	72 Holy Cross Road	(330) 5998696	t
12	Wakefern Food Corporation	pwinrneyb@nature.com	2021-06-27	27929 Commercial Terrace	(180) 4310025	t
13	Geri-Care Pharmaceuticals, Corp	lgiracobinic@phpbb.com	2021-04-11	61 Westerfield Center	(210) 4048622	t
14	Bryant Ranch Prepack	cgrerrsend@bkeley.edu	2020-11-12	0 Mcbride Parkway	(347) 6521256	t
15	Nelco Laboratories, Inc.	kspirrite@discovery.com	2020-12-30	44627 New Castle Circle	(395) 6993781	t
17	Ventura Corporation LTD	srorwetg@amazon.co.jp	2021-02-21	27859 Kings Parkway	(530) 3933459	t
18	KIK Custom Products	dhowbrrookh@army.mil	2020-11-25	268 Debra Plaza	(324) 7954322	t
20	European Perfume Works Co.L.L.C.	binorldj@arizona.edu	2020-12-24	0 Doe Crossing Parkway	(704) 1632740	t
5	Edit Works, Inc.	thinchram4@newsvine.com	2021-08-14	8 Roxbury Trail	(937) 1759600	t
19	Lizabeth Arden, Inc	ghalrpinei@chronoengine.com	2021-06-01	3 Northview Lane	(271) 7728198	t
16	AvPkAK	hkinnrenf@blogtalkradio.com	2021-05-01	30672 Hanover Drive	(815) 8415303	f
21	Severus Snape	lily@gmail.com	1991-07-01	you have your mothers eyes	+963995116817	t
\.


--
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.employee (emp_id, emp_name, email, password, phone, address, title) FROM stdin;
1	Harrie Blackborne	hblackborne0@fotki.com	s8k4Ke1E93wv	437-479-3530	117 Shasta Center	doctor
2	Denni Daviddi	ddaviddi1@de.vu	fGnLKhC	489-251-7086	2832 Lakeland Center	pharmacist
3	Adlai Frowen	afrowen2@bloomberg.com	teEH6oLoG1	680-575-0989	16 Bluejay Street	doctor
4	Reginald Wynch	rwynch3@google.de	iQRlsh1	245-189-0358	83 Hooker Point	pharmacist
5	Trevor Hincham	thincham4@newsvine.com	1ZQzE7	825-690-7906	6644 Ilene Street	doctor
6	Deina Crepel	dcrepel5@home.pl	x0LjeqPGp	323-482-5627	613 Carey Drive	pharmacist
7	Amalea Reen	areen6@apple.com	mNQNmj	582-191-7768	25134 Clove Place	doctor
8	Saunderson Capini	scapini7@ifeng.com	YGouHS1PBv	469-651-1147	2 Stephen Drive	pharmacist
9	Jannelle Smithers	jsmithers8@berkeley.edu	8BHgYEmZEe	455-766-3700	3 Sunbrook Terrace	doctor
10	Ambur MacKettrick	amackettrick9@weebly.com	luSqTmv	767-521-5834	5 Dakota Hill	pharmacist
11	Piper Tappor	ptappora@php.net	Mc40nMr7	643-363-2425	7762 Morningstar Alley	doctor
12	Polly Winney	pwinneyb@nature.com	c6o1KKR9s	773-651-0180	1 Hallows Drive	pharmacist
13	Llewellyn Giacobini	lgiacobinic@phpbb.com	PRiKW3le	155-870-9033	4 Westend Lane	doctor
14	Clerissa Greggersen	cgreggersend@berkeley.edu	9MCSXmWscW	647-273-5213	511 Sachtjen Place	pharmacist
15	Kristyn Spirit	kspirite@discovery.com	xao7Wo7R	816-579-9479	8482 Bartillon Pass	doctor
16	Heindrick Kinnen	hkinnenf@blogtalkradio.com	QJ9Uv6uwycuk	222-282-9550	32 Basil Alley	pharmacist
17	Sterling Rowet	srowetg@amazon.co.jp	yZRDcXkf	325-305-3158	7170 Reinke Avenue	doctor
18	Dexter Howbrook	dhowbrookh@army.mil	h3kNSve4nK	810-371-1459	8949 Westport Crossing	pharmacist
19	Guilbert Halpine	ghalpinei@chronoengine.com	zEhXV8aIK	775-759-1465	547 Butternut Alley	doctor
20	Bartram Inold	binoldj@arizona.edu	H1gcAjpiZ	401-926-3700	0 Nova Drive	pharmacist
21	Kennie Branton	kbrantonk@google.fr	f2D9XupQ	970-490-9561	74245 4th Alley	doctor
22	Codie Deas	cdeasl@washington.edu	nFkfHjalL5	688-660-7230	8 Carioca Point	pharmacist
23	Valentina Iannuzzi	viannuzzim@ezinearticles.com	bu3rIXPsi	307-713-8511	35638 Elgar Center	doctor
24	Babs Edens	bedensn@psu.edu	AHFgtX	739-446-5086	435 Welch Junction	pharmacist
25	Lief Heinssen	lheinsseno@economist.com	rs6FIdPD	219-240-0566	054 Lunder Junction	doctor
26	Agace Braunter	abraunterp@mashable.com	tjsWjIJ	128-638-6396	712 Barby Park	pharmacist
27	Sutton Dwyer	sdwyerq@free.fr	H27Kzcuyhd3	455-953-7552	6 Spohn Plaza	docotor
28	Aubrey Scholfield	ascholfieldr@domainmarket.com	gksNJyk	790-552-8075	92 Monument Point	pharmacist
29	Wake Pirolini	wpirolinis@vinaora.com	4yFegRIHew	922-107-5777	0 Gateway Park	docotor
30	Frasquito Ivashkin	fivashkint@ask.com	DRPSJ4	137-810-0465	79 Larry Center	pharmacist
31	Gladi Maleby	gmalebyu@people.com.cn	rEc2Srsb	314-372-1684	46131 David Point	docotor
32	Allix Eshmade	aeshmadev@dyndns.org	JphsyC	249-856-5095	1979 Glacier Hill Drive	pharmacist
33	Joshua Lammerts	jlammertsw@liveinternet.ru	KLG9908	691-853-1337	94205 Chive Court	docotor
34	Opal Kamena	okamenax@va.gov	oLKTKe9S	976-445-8673	8 Prentice Street	pharmacist
35	Desdemona Colleck	dcollecky@go.com	5VKeoov54	489-901-8785	2 Garrison Place	docotor
36	Poul Dampney	pdampneyz@smh.com.au	LeoubkyoAp	656-255-8293	545 Swallow Road	pharmacist
37	Margarita Gilardoni	mgilardoni10@t-online.de	BHQd4XOxwMSM	352-306-2241	9 Fuller Circle	docotor
38	Berenice Duncan	bduncan11@sun.com	7KGC5Mjesiv	953-515-3846	73083 Rusk Crossing	pharmacist
39	Valentine Hulcoop	vhulcoop12@ibm.com	qIU7xP6aaJWr	655-968-2893	532 Coleman Center	docotor
40	Manfred Elsworth	melsworth13@economist.com	yWGVUr7	744-982-2402	9374 Farmco Street	pharmacist
41	Annabelle Tocher	atocher14@joomla.org	ad6o6V	972-155-0396	2711 Westend Road	docotor
42	Inigo Genicke	igenicke15@livejournal.com	8UhQExQ	908-594-1144	8 Anderson Point	pharmacistr
43	Torey Jewster	tjewster16@unesco.org	G1eY03u	486-340-7525	21310 Aberg Circle	docotor
44	Quint Hendren	qhendren17@economist.com	q666etPu	737-953-8870	95415 Thompson Way	pharmacist
45	Goldina Pohling	gpohling18@si.edu	d64jjolm	515-443-3389	6780 Merrick Way	docotor
46	Estele Blaase	eblaase19@tinyurl.com	PLtB6z24nI4T	421-848-9966	1658 Vermont Hill	pharmacist
47	Brynn Boyall	bboyall1a@economist.com	FAZ7sKaKoyW	907-873-5649	9150 Blaine Plaza	docotor
48	Germana Grix	ggrix1b@cbc.ca	4MwbVmz5	390-384-6721	3745 Raven Parkway	pharmacist
49	Ron Weasly	muggles@hogwarts.com	spiders	333-333-3333	The Borrow	pharmacist
50	Harry Potter	jameslily@hogwarts.com	triwizard	777-777-7777	4 Privet Drive	browser
51	Albus Dumbledore	hogwarts@witchcraft.com	voldymoldy	624-624-6244	12 Grimmauld place	manager
\.


--
-- Data for Name: medicine; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.medicine (drug_name, cmp_name, class, ingredient, prod_date, valid, drug_id, quantity) FROM stdin;
PSORIASIS HP	Kmart Corporation	Arthur Christmas	Clematis Recta Flowering Top, Thyroid	2020-12-11	f	1	26
Endopar	EMD Serono, Inc.	Last Klezmer: Leopold Kozlowski, His Life and Music, The	Aralia racemosa	2021-08-16	f	2	72
ck one airlight pressed powder spf 15	RECORDATI RARE DISEASES, INC.	All at Sea	Titanium Dioxide	2020-12-02	t	3	58
Atenolol	Newton Laboratories, Inc.	Dead or Alive: Hanzaisha	Atenolol	2021-10-03	t	4	53
Fenofibrate	REMEDYREPACK INC.	Dirty Movie	fenofibrate	2021-06-27	f	6	37
Cool Clementine anti-bacterial SCENTED HAND SANITIZER	Torrent Pharmaceuticals Limited	This Is My Father	Alcohol	2021-10-04	f	7	60
healthy accents nighttime cold and flu	Natural Creations, Inc.	Good News	Acetaminophen, Dextromethorphan HBr	2021-04-03	t	8	99
Mirapex	Par Pharmaceutical Inc.	After Innocence	pramipexole dihydrochloride	2020-10-17	t	9	59
RG TD5 Inv.Solid Antiperspirant Deodorant Fresh Blast	WOCKHARDT LIMITED	Pandora and the Flying Dutchman	Aluminum Zirconium Pentachlorohydrex Gly	2021-04-30	f	10	6
Lemon and Basil Antibacterial Foaming Hand Wash	West-ward Pharmaceutical Corp	High Life	Triclosan	2020-12-05	t	11	39
Clinical Works Ocean Breeze Waterless Hand Sanitizer	Wakefern Food Corporation	Project Moon Base	Alcohol	2021-01-13	t	12	84
Oxygen	Geri-Care Pharmaceuticals, Corp	Beyond the Black Rainbow	Oxygen	2021-07-23	f	13	62
mouth fresh	Nelco Laboratories, Inc.	Dream a Little Dream	Cetylpyridinium Chloride	2021-02-21	t	15	36
Foamahol	Ventura Corporation LTD	White Lightning	ALCOHOL	2021-02-19	f	17	82
ESIKA	KIK Custom Products	Can't Buy Me Love	OCTINOXATE	2021-08-27	f	18	97
FertiPlex	European Perfume Works Co.L.L.C.	Hit by Lightning	Agnus castus, Apis Mellifica and Sepia	2021-09-10	t	20	39
Belucie Luxury Essential foundation BB	AvPkAK	Pharaoh's Curse	Titanium Dioxide	2020-12-03	f	16	34
BEING WELL DIAPER RASH CREAMY	Edit Works, Inc.	Love Potion #9	ZINC OXIDE	2021-05-22	t	5	35
Senexon	Lizabeth Arden, Inc	Mrs. Doubtfire	Standardized Senna Concentrate	2021-09-02	t	19	60
Antidote 	Bryant Ranch Prepack	Sun Kissed	Eucalyptol, menthol, methyl salicylate, thymol	2021-06-08	f	14	82
LOreal Paris	WOCKHARDT LIMITED	Evil Turn Good	Greasy Hair	2021-11-19	t	21	0
\.


--
-- Data for Name: records; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.records (drug_id, op_type, rec_date, rec_id, doctor, pharm, quantity) FROM stdin;
1	import	2021-05-24	1	Harrie Blackborne	Denni Daviddi	20
2	export	2020-11-30	2	Adlai Frowen	Reginald Wynch	8
3	import	2021-08-16	3	Trevor Hincham	Deina Crepel	6
4	export	2021-05-30	4	Amalea Reen	Saunderson Capini	20
5	export	2021-03-29	5	Jannelle Smithers	Ambur MacKettrick	17
6	import	2021-07-30	6	Piper Tappor	Polly Winney	12
7	export	2020-10-14	7	Llewellyn Giacobini	Clerissa Greggersen	2
8	export	2020-11-30	8	Kristyn Spirit	Heindrick Kinnen	12
9	import	2021-06-23	9	Sterling Rowet	Dexter Howbrook	3
10	export	2021-06-15	10	Guilbert Halpine	Bartram Inold	13
11	export	2021-02-02	11	Kennie Branton	Codie Deas	19
12	export	2021-09-26	12	Valentina Iannuzzi	Babs Edens	17
14	export	2020-10-24	14	Aubrey Scholfield	Wake Pirolini	5
15	import	2021-03-03	15	Frasquito Ivashkin	Gladi Maleby	5
16	export	2021-08-26	16	Allix Eshmade	Joshua Lammerts	18
17	import	2021-10-06	17	Opal Kamena	Desdemona Colleck	15
18	export	2021-07-11	18	Poul Dampney	Margarita Gilardoni	16
19	import	2020-10-24	19	Berenice Duncan	Valentine Hulcoop	17
20	export	2021-06-09	20	Manfred Elsworth	Annabelle Tocher	17
13	import	2021-04-20	13	Lief Heinssen	Agace Braunter	10
4	export	2021-11-04	22	Amalea Reen	Polly Winney	3
4	export	2021-11-06	23	Adlai Frowen	Ambur MacKettrick	3
\.


--
-- Name: company_cmp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.company_cmp_id_seq', 21, true);


--
-- Name: employee_emp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.employee_emp_id_seq', 1, false);


--
-- Name: medicine_drug_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medicine_drug_id_seq', 21, true);


--
-- Name: records_rec_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.records_rec_id_seq', 23, true);


--
-- Name: company company_cmp_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_cmp_name_key UNIQUE (cmp_name);


--
-- Name: company company_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (cmp_name, email, cmp_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: employee employee_emp_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_emp_name_key UNIQUE (emp_name);


--
-- Name: employee employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (emp_name, emp_id, email, phone) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: medicine medicine_drug_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicine
    ADD CONSTRAINT medicine_drug_id_key UNIQUE (drug_id);


--
-- Name: medicine medicine_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicine
    ADD CONSTRAINT medicine_pkey PRIMARY KEY (drug_name, drug_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: records records_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_pkey PRIMARY KEY (rec_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: medicine medicine_cmp_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicine
    ADD CONSTRAINT medicine_cmp_name_fkey FOREIGN KEY (cmp_name) REFERENCES public.company(cmp_name) ON UPDATE CASCADE;


--
-- Name: records records_doctor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_doctor_fkey FOREIGN KEY (doctor) REFERENCES public.employee(emp_name) ON UPDATE CASCADE;


--
-- Name: records records_drug_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_drug_id_fkey FOREIGN KEY (drug_id) REFERENCES public.medicine(drug_id) ON UPDATE CASCADE;


--
-- Name: records records_pharm_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_pharm_fkey FOREIGN KEY (pharm) REFERENCES public.employee(emp_name) ON UPDATE CASCADE;


--
-- PostgreSQL database dump complete
--
