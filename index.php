<?php
include("connection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Login</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/login.css">
</head>
<body>
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class="card login-card">
        <div class="row no-gutters">
          <div class="col-md-5">
            <img src="assets/img/login.jpg" alt="login" class="login-card-img">
          </div>
          <div class="col-md-7">
            <div class="card-body">
              <div class="brand-wrapper">
                <img src="assets/img/logo.svg" alt="logo" class="logo">
              </div>
              <p class="login-card-description">Sign into your account</p>
              <form method="post">
                  <div class="form-group">
                    <label for="email" class="sr-only">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email address">
                  </div>
                  <div class="form-group mb-4">
                    <label for="password" class="sr-only">Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="***********">
                  </div>
                  <button name="btn" id="login" class="btn btn-block login-btn mb-4 button" value="GO">Login</button>
                </form>
                <?php
                //incase the button is clicked call the login function
                if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['btn'])){
                          login();
                }
                function login(){
                  //take the password and the email
                  $email = $_POST['email'];
                  $password = $_POST['password'];
                  $Obj = new dbObj();
                  $con = $Obj->getConnstring();
                  /* check connection */
                  if (pg_last_error()) {
                    printf("Connect failed: %s\n", pg_last_error());
                    exit();
                  }
                  //searrch in the employee table for a password and a name like that
                  $sql = "SELECT * FROM employee WHERE email='$email' AND password='$password' ";
                  //run query
                  $result = pg_query($con,$sql) or die("Query unsuccessful, Couldn't log in :/") ;
                  // incase the rows are not zero
                  $incorrect = false;
                  if (pg_num_rows($result) > 0) { //there's one employee with these info
                  $title = pg_fetch_result($result,0,'title') ;
                  //the title will determine the page the user is redirected to
                    if($title == "manager"){
                      //manager\manager.php
                      header('Location: manager/manager.php');
                    }else if($title == "pharmacist"){
                      header('Location: pharmacist/pharmacist.php');
                    }else{
                      header('Location: browser/browser.php');
                    }
                    //else there's no employee with both constraints, a mistake must have occured
                  }else{//debug which field is the wrong one
                      $sql = "SELECT * FROM employee WHERE email='$email'  ";//less conditions, only check email
                      $result = pg_query($con,$sql) or die("Query unsuccessful") ;//run query
                      if (pg_num_rows($result) > 0) {//if it exists, then password is wrong
                        echo "<p style='color:red; text-align:left;' >Password is incorrect</p>";
                        $incorrect = true;
                      }
                      $sql = "SELECT * FROM employee WHERE password='$password' ";//less conditions, only check password
                      $result = pg_query($con,$sql) or die("Query unsuccessful") ;//run query
                      if (pg_num_rows($result) > 0) {//if it exists, then password is wrong
                        echo "<p style='color:red; text-align:left;' >Email is incorrect</p>";
                        $incorrect = true;
                      }
                    }
                    if(!$incorrect){
                      echo "<p style='color:red; text-align:left;' >Email and Password are incorrect</p>";
                    }
                }
                ?>
                <a href="#!" class="forgot-password-link">Forgot password?</a>
                <p class="login-card-footer-text">Don't have an account? <a href="#!" class="text-reset">Register here</a></p>
                <nav class="login-card-footer-nav">
                  <a href="#!">Terms of use.</a>
                  <a href="#!">Privacy policy</a>
                </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>
