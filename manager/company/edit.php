<?php
include("../../connection.php");
include("company.php");
$id = $_GET["id"];
$newObj = new Company();
$cmps = $newObj->fetchCmp($id);
$name="";
$email="";
$address="";
$date="";
$phone="";
 foreach($cmps as $key => $cmp) :
   $name=$cmp['cmp_name'];
   $email=$cmp['email'];
   $address=$cmp['address'];
   $date=$cmp['contract_date'];
   $phone=$cmp['phone'];
 endforeach;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="../../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="../../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="../../assets/css/form.css" rel="stylesheet">
  </head>
  <body>
    <!-- ======= Header ======= -->
    <header id="header" class="d-flex flex-column justify-content-center">

      <nav id="navbar" class="navbar nav-menu">
        <ul>
          <li><a href="../manager.php" class="nav-link scrollto active"><i class="fas fa-building"></i><span>Home</span></a></li>
          <li><a href="../../index.php" class="nav-link scrollto"><i class="fas fa-sign-out-alt"></i> <span>Log out</span></a></li>

        </ul>
      </nav><!-- .nav-menu -->

    </header><!-- End Header -->
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-body">
                    <h2 class="title"> Update Company Info</h2>
                    <form method="POST">
                        <div class="input-group">
                            <input name="name" class="input--style-1" type="text" placeholder="Pharamaceutical Company" value="<?php echo $name; ?>" required >
                        </div>
                        <div class="input-group">
                          <input name="email" class="input--style-1" type="email" placeholder="E-mail" value="<?php echo $email; ?>" required>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input name="date" class="input--style-1 js-datepicker" type="text" placeholder="Contract Date" value="<?php echo $date; ?>" required >
                                    <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                </div>
                            </div>
                            <div class="col-2">
                              <div class="input-group">
                                  <input name="address" class="input--style-1" type="text" placeholder="Address" value="<?php echo $address; ?>" required>
                              </div>
                            </div>
                        </div>

                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                  <input name="phone" class="input--style-1" type="text" placeholder="Phone" value="<?php echo $phone; ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="p-t-20">
                            <button name="update" class="btn btn--radius btn--green" type="submit">Edit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ======= Footer ======= -->
    <footer id="footer">
      <div class="container">
        <div class="copyright">
          &copy; YaraKK <strong><span>AmesCom Trainee</span></strong>. All Rights Reserved
        </div>
        </div>
    </footer><!-- End Footer -->


      <div id="preloader"></div>
      <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

      <!-- Vendor JS Files -->
      <script src="../../assets/vendor/aos/aos.js"></script>
      <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
      <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
      <script src="../../assets/vendor/php-email-form/validate.js"></script>
      <script src="../../assets/vendor/purecounter/purecounter.js"></script>
      <script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>
      <script src="../../assets/vendor/typed.js/typed.min.js"></script>
      <script src="../../assets/vendor/waypoints/noframework.waypoints.js"></script>

      <!-- Template Main JS File -->
      <script src="../../assets/js/main.js"></script>
      <script src="../../assets/js/table.js"></script>
  </body>
  <?php
  if(isset($_POST['update'])){
      $newObj->updateCmp($id);
?>
<script type="text/javascript">
  //update without refreshing
  window.location = "../manager.php";
</script>

<?php
}
?>
</html>
