<?php
 //to access postgres database connection object.
  class Company {
    protected $conn;
    protected $data = array();
    function __construct() {
      $db = new dbObj();
      $connString =  $db->getConnstring();
      $this->conn = $connString;
    }
    //to access company data from postgres database and return it.
    public function getCmp() {
      $sql = "SELECT  cmp_id, cmp_name, email, address, contract_date, phone  FROM company WHERE valid=true";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Get Company Data");
      $data = pg_fetch_all($queryRecords);
      return $data;
    }
    //update a company record by info from add.php
    public function updateCmp($id){
      $name=$_POST['name'];
      $email=$_POST['email'];
      $address=$_POST['address'];
      $date=$_POST['date'];
      $phone=$_POST['phone'];
      $sql = "UPDATE company SET(cmp_name, email, address, contract_date, phone) = ('$name', '$email', '$address', '$date', '$phone') WHERE cmp_id='$id'";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Update Company Data");// select query
    }
    //fetch a record from company table by id
    public function fetchCmp($id){
        $sql = "SELECT * FROM company WHERE cmp_id=$id";
        $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Fetch Company Data");// select query
        $data = pg_fetch_all($queryRecords);
        return $data;
    }
    //insert a new company record
    public function insertCmp(){
      $sql = "INSERT INTO company VALUES(DEFAULT,'$_POST[name]', '$_POST[email]', '$_POST[date]', '$_POST[address]', '$_POST[phone]',true)";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Insert Company Data");// select query
    }
    //search the value in all columns and format the date to be a date
    public function search($search){
      $sql = "SELECT * FROM company WHERE valid=true AND (cmp_name LIKE '%".$search."%' OR email LIKE '%".$search."%' OR contract_date::text LIKE '%".$search."%' OR address LIKE '%".$search."%'OR phone LIKE '%".$search."%') ";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Search");
      if (pg_num_rows($queryRecords) == 0){
        return 0;
      }
      $data = pg_fetch_all($queryRecords);
      return $data;
    }
    //delet by setting valid to false
    public function deleteCmp($id){
      $sql = "UPDATE company SET valid = false WHERE cmp_id=$id";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Delete Comapny Data");// select query
    }


  }
 ?>
