<?php
 //to access postgres database connection object.
  class Meds {
    protected $conn;
    protected $data = array();
    function __construct() {
      //intitialize a connection var to connect database
      $db = new dbObj();
      $connString =  $db->getConnstring();
      $this->conn = $connString;
    }
    //to access medicine data from postgres database and return it.
    public function getMed() {
      $sql = "SELECT  drug_id, drug_name, cmp_name, class, ingredient, prod_date  FROM medicine WHERE valid=true";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Get medicine Data");
      $data = pg_fetch_all($queryRecords);
      return $data;
    }
    //update a single med by id from edit.php form
    public function updateMed($id){
      $name=$_POST['name'];
      $company=$_POST['company'];
      $class=$_POST['class'];
      $ingred=$_POST['ingred'];
      $date=$_POST['date'];
      $sql = "UPDATE medicine SET(drug_name, cmp_name, class, ingredient, prod_date) = ('$name', '$company', '$class', '$ingred', '$date') WHERE drug_id='$id'";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Update medicine Data");// select query
    }
    //fetch a single medicine from table by id
    public function fetchMed($id){
        $sql = "SELECT * FROM medicine WHERE drug_id=$id";
        $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Fetch medicine Data");// select query
        $data = pg_fetch_all($queryRecords);
        return $data;
    }
    //add a med to the med table from add.php form with the valid value to be true and the id is set by default and quantitiy to be zero
    public function insertMed(){
      $sql = "INSERT INTO medicine VALUES('$_POST[name]','$_POST[company]','$_POST[class]', '$_POST[ingred]', '$_POST[date]', true, DEFAULT, 0 )";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Insert medicine Data");// select query
    }
    //search a value in all med table columns and convert the date format to text to avoid errors
    public function search($search){
      $sql = "SELECT * FROM medicine WHERE valid=true AND (drug_name LIKE '%".$search."%' OR cmp_name LIKE '%".$search."%' OR prod_date::text LIKE '%".$search."%' OR class LIKE '%".$search."%'OR ingredient LIKE '%".$search."%') ";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Search");
      if (pg_num_rows($queryRecords) == 0){
        return 0;
      }
      $data = pg_fetch_all($queryRecords);
      return $data;
    }
    //delete the med is done by changing the value of valid to false therefore it doesn't show on table
    public function deleteMed($id){
      $sql = "UPDATE medicine SET valid = false WHERE drug_id=$id";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Delete medicine Data");// select query
    }
  }
 ?>
