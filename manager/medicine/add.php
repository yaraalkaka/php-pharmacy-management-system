<?php
include("../../connection.php");

  include("medicine.php");
  $newObj = new Meds();

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Add</title>
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="../../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">
  <link href="../../assets/css/form.css" rel="stylesheet">

</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex flex-column justify-content-center">

    <nav id="navbar" class="navbar nav-menu">
      <ul>
        <li><a href="../manager.php" class="nav-link scrollto active"><i class="fas fa-building"></i><span>Home</span></a></li>
        <li><a href="../../index.php" class="nav-link scrollto"><i class="fas fa-sign-out-alt"></i> <span>Log out</span></a></li>

      </ul>
    </nav>
    <!-- .nav-menu -->

  </header>
  <!-- End Header -->
  <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
    <div class="wrapper wrapper--w680">
      <div class="card card-1">
        <div class="card-body">
          <h2 class="title">Add a New Medicine</h2>
          <form method="POST">
            <div class="input-group">
              <input name="name" class="input--style-1" type="text" placeholder="Medicine Name" required>
            </div>
            <div class="input-group">
              <div class="rs-select2 js-select-simple select--no-search">
                <select name="company" style="border:none; outline: none;">
                  <option disabled="disabled" selected="selected">Company Name</option>
                  <!-- get company's name to show on dropdown list to pick an exisiting company -->
                  <?php
                                    //only add a medicine from a valid company
                                    $sql = "SELECT * FROM company WHERE valid=true";
                                    $Obj = new dbObj();
                                    $con = $Obj->getConnstring();
                                    $queryRecords = pg_query($con, $sql) or die("Couldn't Fetch Company Data");// select query
                                    $cmps = pg_fetch_all($queryRecords);
                                    foreach($cmps as $key => $cmp) :?>
                  <option><?php echo $cmp['cmp_name']; ?></option>
                  <?php endforeach;  ?>
                </select>
                <div class="select-dropdown"></div>
              </div>
            </div>
            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <input name="class" class="input--style-1 js-datepicker" type="text" placeholder="Classification" required>
                  <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                </div>
              </div>
              <div class="col-2">
                <div class="input-group">
                  <input name="ingred" class="input--style-1" type="text" placeholder="Ingrediants" required>
                </div>
              </div>
            </div>

            <div class="row row-space">
              <div class="col-2">
                <div class="input-group">
                  <input name="date" class="input--style-1" type="date" placeholder="Production Date" required>
                </div>
              </div>
            </div>
            <div class="p-t-20">
              <button name="insert" class="btn btn--radius btn--green" type="submit">Add</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; YaraKK <strong><span>AmesCom Trainee</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer>
  <!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>



  <!-- Vendor JS Files -->
  <script src="../../assets/vendor/aos/aos.js"></script>
  <script src="../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="../../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../../assets/vendor/php-email-form/validate.js"></script>
  <script src="../../assets/vendor/purecounter/purecounter.js"></script>
  <script src="../../assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="../../assets/vendor/typed.js/typed.min.js"></script>
  <script src="../../assets/vendor/waypoints/noframework.waypoints.js"></script>

  <!-- Template Main JS File -->
  <script src="../../assets/js/main.js"></script>
  <script src="../../assets/js/table.js"></script>

  <!-- if we set the insert button then run the insert function -->
  <?php
    if(isset($_POST['insert'])){
        $newObj->insertMed();
  ?>
  <!-- when done successfully redicrect to home page -->
  <script type="text/javascript">
    //update without refreshing
    window.location = "../manager.php";
  </script>
  <?php
  }
  ?>
</body>

</html>
