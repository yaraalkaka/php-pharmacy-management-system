<?php
include("../connection.php");
  include("company/company.php");
  include("medicine/medicine.php");
  $newObj1 = new Company();
  $cmps=$newObj1->getCmp();
  $newObj2 = new Meds();
  $meds=$newObj2->getMed();

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Manager</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../assets/img/logo.png" rel="icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="../assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
  <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="crossorigin="anonymous"></script>

<!--===============================================================================================-->

<!-- table -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/281e4856c4.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>

<!-- Template Main CSS File -->
<link href="../assets/css/style.css" rel="stylesheet">
<link href="../assets/css/table.css" rel="stylesheet">

</head>
<body>
  <!-- ======= Header ======= -->
  <header id="header" class="d-flex flex-column justify-content-center">
    <!-- .nav-menu -->
    <nav id="navbar" class="navbar nav-menu">
      <ul>
        <li><a href="#company" class="nav-link scrollto active"><i class="fas fa-building"></i><span>Companies Table</span></a></li>
        <li><a href="#meds" class="nav-link scrollto"><i class="fas fa-pills"></i> <span>Meds Table</span></a></li>
        <li><a href="../index.php" class="nav-link scrollto"><i class="fas fa-sign-out-alt"></i> <span>Log out</span></a></li>

      </ul>
    </nav>
  </header>
  <!-- End Header -->
  <!-- ======= Company Section ======= -->
  <section id="company" class="d-flex flex-column justify-content-center">
      <h1>Pharmaceutical Companies</h1>
      <div class="container-lg">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <!-- Table Header -->
                  <!-- Table Name -->
                  <div class="col-sm-4"><h2>Company <b>Details</b></h2></div>
                  <!-- Add Row Button -->
                  <div class="col-sm-8 add_new">
                      <a href="company/add.php" class="btn btn-info add-new"><i class="fa fa-plus"></i>Add New</a>
                  </div>
              </div>
            </div>
              <!-- Company Table -->
                <table id='company_table' class='table table-bordered'>
                    <thead>
                        <tr>
                        <!-- we have an anchor tag to get header info when clicked to get the table ordered by it -->
                            <th>Pharamaceutical Company<br>
                                Email</th>
                            <th>Address</th>
                            <th>Contract Date</th>
                            <th>Phone</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                  <?php
                  foreach($cmps as $key => $cmp) :?>
                    <tr>
                      <td> <?php echo $cmp['cmp_name']; ?> <br><p style='color: #718096; font-size: 12px;'>
                           <?php echo $cmp['email']; ?> </p></td>
                      <td> <?php echo $cmp['address']; ?> </td>
                      <td> <?php echo $cmp['contract_date']; ?> </td>
                      <td> <?php echo $cmp['phone']; ?> </td>
                      <td>
                          <a href="company/edit.php?id=<?php echo $cmp['cmp_id']; ?> " class='edit' title='Edit' data-toggle='tooltip'><i class='material-icons'>&#xE254;</i></a>
                          <a href="company/delete.php?id=<?php echo $cmp['cmp_id']; ?>" class='delete' title='Delete' data-toggle='tooltip'><i class='material-icons'>&#xE872;</i></a>
                      </td>
                        <?php endforeach;?>
                    </tr>
                      </tbody>
                    </table>
          </div>
        </div>
      </div>
  </section>
  <!-- End company -->

    <!-- ======= Meds Section ======= -->
    <section id="meds" class="meds">
      <h1>Medications</h1>
      <div class="container-lg">
        <div class="table-responsive">
          <div class="table-wrapper">
              <div class="table-title">
                  <div class="row">
                    <!-- Table Header -->
                      <!-- Table Name -->
                      <div class="col-sm-4"><h2>Medicines <b>Details</b></h2></div>

                      <!-- add row button -->
                      <div class="col-sm-8 add_new">
                          <a href="medicine/add.php" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</a>
                      </div>
                  </div>
              </div>
              <!-- Med table -->
                <table id='meds_table' class='table table-bordered'>
                    <thead>
                        <tr>
                        <!-- we have an anchor tag to get header info when clicked to get the table ordered by it -->
                            <th>Medicine Name<br>
                                Company Name</th>
                            <th>Classification</th>
                            <th>Ingrediants</th>
                            <th>Production Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                  foreach($meds as $key => $med) :?>
                    <tr>
                      <td> <?php echo $med['drug_name']; ?> <br><p style='color: #718096; font-size: 12px;'>
                           <?php echo $med['cmp_name']; ?>  </p></td>
                      <td> <?php echo $med['class']; ?> </td>
                      <td> <?php echo $med['ingredient']; ?>  </td>
                      <td> <?php echo $med['prod_date']; ?> </td>
                      <td>
                          <a href='medicine/edit.php?id=<?php echo $med['drug_id']; ?> ' class='edit' title='Edit' data-toggle='tooltip'><i class='material-icons'>&#xE254;</i></a>
                          <a href='medicine/delete.php?id=<?php echo $med['drug_id']; ?>' class='delete' title='Delete' data-toggle='tooltip'><i class='material-icons'>&#xE872;</i></a>
                      </td>
                        <?php endforeach;?>
                    </tr>
                    </tbody>
              </table>
          </div>
        </div>
      </div>
    </section>
<!-- End Meds Section -->


  <!-- ======= Footer ======= -->
<footer id="footer">
  <div class="container">
    <div class="copyright">
      &copy; YaraKK <strong><span>AmesCom Trainee</span></strong>. All Rights Reserved
    </div>
  </div>
</footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <script>
      $(document).ready( function () {
      $('#company_table').DataTable();
      } );
      $(document).ready( function () {
      $('#meds_table').DataTable();
      } );
  </script>
  <!-- Vendor JS Files -->
  <script src="../assets/vendor/aos/aos.js"></script>
  <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../assets/vendor/php-email-form/validate.js"></script>
  <script src="../assets/vendor/purecounter/purecounter.js"></script>
  <script src="../assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="../assets/vendor/typed.js/typed.min.js"></script>
  <script src="../assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="jquery.min.js" charset="utf-8"></script>
  <!-- Template Main JS File -->
  <script src="../assets/js/table.js"></script>
  <script src="../assets/js/main.js"></script>


</body>

</html>
