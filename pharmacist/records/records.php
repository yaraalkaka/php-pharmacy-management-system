<?php
 //to access postgres database connection object.
  class Records {
    protected $conn;
    protected $data = array();
    function __construct() {
      $db = new dbObj();
      $connString =  $db->getConnstring();
      $this->conn = $connString;
    }
    //to access records data from postgres database and return it.
    public function getRec() {
      $sql = "SELECT  rec_id, pharm, doctor, op_type, rec_date, drug_id, quantity  FROM records";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Get Records Data");
      $data = pg_fetch_all($queryRecords);
      return $data;
    }
    public function updateRec($id){
      $p_name=$_POST['pharm'];
      $d_name=$_POST['doctor'];
      $op=$_POST['op_type'];
      $date=$_POST['rec_date'];
      $id=$_POST['drug_id'];
      $quan=$_POST['quantity'];
      $sql = "UPDATE records SET(pharm, doctor, op_type, rec_date, drug_id,quantity) = ('$p_name', '$d_name', '$op', '$date', '$id', '$quan') WHERE rec_id='$id'";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Update Records Data");// select query
    }
    public function fetchRec($id){
        $sql = "SELECT * FROM records WHERE rec_id=$id";
        $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Fetch Records Data");// select query
        $data = pg_fetch_all($queryRecords);
        return $data;
    }
    public function insertRec(){
      $sql = "INSERT INTO records VALUES('$_POST[drug_id]', '$_POST[op_type]', '$_POST[rec_date]', DEFAULT,'$_POST[doctor]', '$_POST[pharm]', '$_POST[quantity]')";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Insert Records Data");// select query
    }
    public function search($search){
      $sql = "SELECT * FROM records WHERE pharm LIKE '%".$search."%' OR doctor LIKE '%".$search."%' OR rec_date::text LIKE '%".$search."%' OR op_type LIKE '%".$search."%'OR drug_id LIKE '%".$search."%'OR quantity LIKE '%".$search."%' ";
      $queryRecords = pg_query($this->conn, $sql) or die("Couldn't Search");
      $data = pg_fetch_all($queryRecords);
      return $data;
    }

  }
 ?>
